#!/usr/bin/bash

rm -rf CMakeFiles/
rm -rf build/
rm -rf bin/
rm -f Makefile
rm -f CMakeCache.txt
rm -f cmake_install.cmake
echo "Clean!"
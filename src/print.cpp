#include "DIMClient/print.h"
#include <sys/time.h>
#include <time.h>
#include <sstream>
#include <math.h>
#include <iomanip>

void Print(const string type, const string message)
{
	struct timeval tv;
	gettimeofday(&tv, NULL);

	int micros = tv.tv_usec;
	while (micros > 1000000)
	{
		micros -= 1000000;
		tv.tv_sec++;
	}

	struct tm* tm_info = localtime(&tv.tv_sec);
	char buffer[40];
	strftime(buffer, 40, "%Y:%m:%d %H:%M:%S", tm_info);

	cout << '[' << type << "] [" << buffer << "." << setw(6) << setfill('0') << micros  << "] " << message << '\n';
}

void PrintError(string message)
{
	Print(string(ANSI_COLOR_RED) + "ERROR" + ANSI_COLOR_RESET, message);
}

void PrintWarning(string message)
{
	Print(string(ANSI_COLOR_YELLOW) + "WARNING" + ANSI_COLOR_RESET, message);
}

void PrintInfo(string message)
{
	Print(string(ANSI_COLOR_GREEN) + "INFO" + ANSI_COLOR_RESET, message);
}

void PrintVerbose(string message)
{
	Print(string(ANSI_COLOR_BLUE) + "VERBOSE" + ANSI_COLOR_RESET, message);
}

void PrintSeparationLine()
{
	int width = 89;
	cout << setfill ('=') << setw (width) << "" << endl;
}

#include <signal.h>
#include <boost/program_options.hpp>
#include "DIMClient/print.h"
#include "DIMClient/utility.h"
#include "DIMClient/dimutilities.h"
#include "DIMClient/callbacks.h"
#include "DIMClient/dimclient.h"

/*
 * DimClient constructor
 */
Dimclient::Dimclient(int argc, char** argv)
{
    signal(SIGINT, &terminate);

    namespace po = boost::program_options;
    po::options_description description("DIMClient options");
    description.add_options()
        ("help, h", "Print help message")
        ("dns, d", po::value<string>(), "DIM_DNS_NODE")
        // ("verbose, v", "Verbose output")
        ("interactive, i", "Verbose output")
        ("ALF, A", "ALF mode")
        ("FRED, F", "FRED mode")
        ("topic, t", po::value<string>(), "Topic name") // should be service name
        ("message, m", po::value<string>(), "Topic message");

    po::variables_map vm;

    try
    {
        po::store(po::parse_command_line(argc, argv, description), vm);

        if (vm.count("help"))
        {
            cout << description << endl;
            exit(EXIT_SUCCESS);
        }

        if (vm.count("dns"))
        {
            dns = vm["dns"].as<string>();
            setenv("DIM_DNS_NODE", string(dns).c_str(), 1);
            PrintInfo("DIM_DNS_NODE set to " + dns);
        }
        else 
        {
            if (getenv(string("DIM_DNS_NODE").c_str()) != nullptr)
            {
                dns = getenv(string("DIM_DNS_NODE").c_str());
                PrintInfo("DIM_DNS_NODE set to " + dns + " from environmental variable");
            }
            else
            {
                PrintError("DIM_DNS_NODE not set! Use argument -dns <DIM_DNS_NODE> or environmental variable DIM_DNS_NODE");
                exit(EXIT_FAILURE);
            }
        }

        if (!DimUtilities::dimDnsIsUp(dns))
        {
            PrintError("DIM DNS " + dns + " is NOT UP!");
            exit(EXIT_FAILURE);
        }

        if (vm.count("interactive"))
        {
            PrintInfo("DIMClient is in interactive mode!");
            interactive = true;
        }
        else // Non interactive mode
        {   
            if (vm.count("ALF"))
            {
                PrintInfo("DIMClient is in ALF mode!");
                ALFmode = true;
            }
            else if (vm.count("FRED"))
            {
                PrintInfo("DIMClient is in FRED mode!");
                FREDmode = true;
            }
            else
            {
                PrintInfo("DIMClient is in standard mode, no suffixes will be applied!");
            }

            if (vm.count("topic"))
            {
                topic = vm["topic"].as<string>();
                PrintInfo("Topic name: " + topic);
            }
            else
            {
                PrintError("Topic name not set!");
                exit(EXIT_FAILURE);
            }

            if (vm.count("message"))
            {
                message = vm["message"].as<string>();
                replace(message.begin(), message.end(), '_', '\n'); // new line cannot be parsed
                PrintInfo("Topic message:\n" + message);
            }
            else
            {
                PrintWarning("Topic message not set, sending empty string!");
                message = "";   
            }
        }

        po::notify(vm);
    }
    catch (po::error& e)
    {
        PrintError(e.what());
        cerr << description << endl;
        exit(EXIT_SUCCESS);
    }
}

void Dimclient::run()
{
    extern bool waiting;
    extern bool error;

    if (interactive)
    {
        while (1)
        {
            vector<vector <string> > servers = DimUtilities::retrieveServers();
            sort(servers.begin(), servers.end(), alphaSort);
            Utility::printServers(servers);

            PrintSeparationLine();
            cout << "q -> exit" << endl << "r -> refresh" << endl;
            cout << "Enter server number: " << endl;
            string ans;
            cin >> ans;
            PrintSeparationLine();
            
            if (ans == "q") exit(0);
            else if (ans == "r") continue;
            
            string server = servers[stoi(ans)][0];
            
            while (1)
            {
                vector<vector <string> > services = DimUtilities::retrieveServices(server);
                sort(services.begin(), services.end(), alphaSort);
                analyzeServices(services); // set ALFmode/FREDmode
                Utility::printServices(services);

                PrintSeparationLine();
                cout << "q -> back, change server" << endl << "r -> refresh" << endl << "m -> monitor" << endl;
                cout << "Enter service number: " << endl;
                cin >> ans;
                PrintSeparationLine();

                int serviceChoice;
                if (ans == "q") break; 
                else if (ans == "r") continue;
                else if (ans == "m") // monitor mode
                {
                    vector<std::unique_ptr<Callback>> ansVector;
                    vector<std::unique_ptr<ERRCallback>> errVector;

                    if (FREDmode)
                    {
                        for (size_t i = 0; i < services.size(); i ++)
                        {
                            topic = services[i][0];
                            ansVector.push_back(std::make_unique<Callback>(topic + "_ANS"));
                            errVector.push_back(std::make_unique<ERRCallback>(topic));
                        }
                    }
                    else PrintError("Monitor is available in FRED mode only! (--F)");

                    while (1)
                    {
                        // ctrl+c to exit monitor mode (?)
                    }
                }
                else serviceChoice = stoi(ans); // can be better

                while (1)
                {
                    PrintSeparationLine();
                    cout << "q -> back, change service" << endl << "e -> send empty message" << endl << "r -> repeat" << endl;
                    cout << "Enter message: " << endl;
                    cin >> ans;
                    PrintSeparationLine();

                    if (ans == "e") message = "";
                    else if (ans == "q") break;
                    else if (ans == "r")
                    {
                        // repeat
                    }
                    else message = ans;

                    if (message == "") PrintInfo("Sending empty message");
                    else
                    {
                        replace(message.begin(), message.end(), '_', '\n'); // new line cannot be parsed
                        PrintInfo("Sending message:\n" + message);                      
                    }

                    topic = services[serviceChoice][0];

                    if (ALFmode)
                    {
                        waiting = true; // alternative to mutex

                        Callback *callback = new Callback(topic + "/RpcOut");

                        usleep(1000); // needed by DIM

                        DimUtilities::sendDIMCommand(topic + "/RpcIn", message);

                        while (waiting)
                        {
                            // wait for one of the callbacks
                        }

                        usleep(1000); // needed by DIM

                        delete callback;
                    }
                    else if (FREDmode)
                    {
                        // topic.erase(topic.size() - 4); // remove "_REQ"

                        waiting = true; // alternative to mutex

                        Callback *callback = new Callback(topic + "_ANS");
                        ERRCallback *errCallback = new ERRCallback(topic);

                        usleep(1000); // needed by DIM

                        DimUtilities::sendDIMCommand(topic + "_REQ", message);
    
                        while (waiting)
                        {
                            // wait for one of the callbacks
                        }

                        usleep(1000); // needed by DIM

                        delete errCallback;
                        delete callback;
                    }
                    else
                    {
                        waiting = true; // alternative to mutex

                        Callback *callback = new Callback(topic);

                        usleep(1000); // needed by DIM

                        DimUtilities::sendDIMCommand(topic, message);

                        while (waiting)
                        {
                            // wait for one of the callbacks
                        }

                        usleep(1000); // needed by DIM

                        delete callback;
                    }
                }   
            }
        }   
    }
    else
    {
        if (FREDmode)
        {
            Callback *callback = new Callback(topic + "_ANS");
            ERRCallback *errCallback = new ERRCallback(topic);
        }
        else if (ALFmode) Callback *callback = new Callback(topic + "/RpcOut");
        else Callback *callback = new Callback(topic);

        usleep(1000); // needed by DIM

        if (FREDmode) DimUtilities::sendDIMCommand(topic + "_REQ", message);
        else if (ALFmode) DimUtilities::sendDIMCommand(topic + "/RpcIn", message);
        else DimUtilities::sendDIMCommand(topic, message);

        while (waiting)
        {
            // wait for one of the callbacks
        }
    }

    if (error) exit(EXIT_FAILURE);
    else exit(EXIT_SUCCESS);
}

/*
 * Analyze a service list to determine if the server is ALF or FRED
 */
vector<vector <string> > Dimclient::analyzeServices(vector<vector <string> > &services)
{
    vector<vector <string> > analyzed;

    bool F, FR, FRED;
    bool AL, ALF;

    for (size_t i = 0; i < services.size(); i ++)
    {
        string name = services[i][0];

        if (name.find("_REQ") == name.size() - 4) F = true;
        if (name.find("_ERR") == name.size() - 4) FR = true;
        if (name.find("_ANS") == name.size() - 4) FRED = true;

        if (name.find("SCA_SEQUENCE") !=string::npos) AL = true;
        if (name.find("SWT_SEQUENCE") !=string::npos) ALF = true;
    }

    if (F && FR && FRED)
    {
        FREDmode = true;
        ALFmode = false;
        PrintInfo("FRED discovered!");
    }
    else if (AL && ALF)
    {
        FREDmode = false;
        ALFmode = true;
        PrintInfo("ALF discovered!");
    }
    else
    {
        PrintWarning("Neither ALF nor FRED detected!");
        PrintWarning("DIMClient has not been tested outside ALFRED!");
        PrintWarning("Good luck!");
    }

    for (size_t i = 0; i < services.size(); i ++)
    {
        string name = services[i][0];
        if (FREDmode)
        {
            if (name.find("_REQ") == name.size() - 4)
            {
                services[i][0].erase(services[i][0].size() - 4); // remove "_REQ"
                analyzed.push_back(services[i]);
            }
        }
        else analyzed.push_back(services[i]); // ALF or generic server
    }

    services = analyzed;
    return analyzed;
}

void Dimclient::terminate(int)
{
    PrintWarning("Closing DimClient!");
    exit(EXIT_SUCCESS);
}

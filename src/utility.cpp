#include <iomanip>
#include <cctype>
#include <algorithm>
#include <string>
#include <climits>
#include "DIMClient/utility.h"
#include "DIMClient/dimutilities.h"
#include "DIMClient/print.h"

void Utility::printServers(vector<vector <string> > servers)
{
	PrintSeparationLine();
	for (size_t i = 0; i < servers.size(); i ++)
	{
		cout << setfill(' ') << setw(4) << left << i;
		cout << setfill(' ') << setw(45) << left << servers[i][0]; // name
		cout << setfill(' ') << setw(30) << left << servers[i][1];
		cout << setfill(' ') << setw(10) << right << servers[i][2];
		cout << endl;
	}
	PrintSeparationLine();
}

void Utility::printServices(vector<vector <string> > services)
{
	PrintSeparationLine();
	for (size_t i = 0; i < services.size(); i ++)
	{
		cout << setfill(' ') << setw(4) << left << i;
		cout << setfill(' ') << setw(70) << left << services[i][0];
		cout << setfill(' ') << setw(15) << right << services[i][1];
		cout << endl;
	}
	PrintSeparationLine();
}

/*
 * Simple alphabethic sort for servers and services list
 */
bool alphaSort(vector<string> a, vector<string> b)
{
	vector<string> unsorted, sorted;
	unsorted.push_back(a[0]);
	unsorted.push_back(b[0]);   
	sorted.push_back(a[0]);
	sorted.push_back(b[0]);

	sort(sorted.begin(), sorted.end());

	return sorted[0] == unsorted[0];
}

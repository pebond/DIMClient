#ifndef UTILITY_H
#define UTILITY_H

#include <vector>
#include <string>

using namespace std;
	
bool alphaSort(vector<string> a, vector<string> b);

class Utility
{
public:
	static string randomString(int width);

	static void printServers(vector<vector <string> > servers);
	static void printServices(vector<vector <string> > services);
};

#endif // UTILITY_H

#ifndef DIM_UTILITIES_H
#define DIM_UTILITIES_H

#include <vector>
#include <string>

using namespace std;

class DimUtilities
{
public:
	static vector<vector <string> > retrieveServers();
	static vector<vector <string> > retrieveServices(string server);

	static bool dimDnsIsUp(string dns);
	static bool serverIsUp(string server);
	static bool serviceIsUp(string server, string service);

	static void sendDIMCommand(string topic, string message);
};

#endif // DIM_UTILITIES_H

#ifndef DIMCLIENT_H
#define DIMCLIENT_H

#include <string>

using namespace std;

class Dimclient
{
private:
    static void terminate(int);
    
    bool interactive = false;
    
    bool ALFmode = false;
    bool FREDmode = false;
    
    string dns;
    string topic, message;

public:
    Dimclient(int argc, char** argv);
    void run();
    vector<vector <string> > analyzeServices(vector<vector <string> > &services);
};

#endif // DIMCLIENT_H

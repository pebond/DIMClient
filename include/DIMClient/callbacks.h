#ifndef CALLBACKS_H
#define CALLBACKS_H

#include <iostream>
#include <string>
#include <vector>
#include <dim/dic.hxx>

extern bool waiting;
extern bool error;

char nofred[30] = "NO DIM LINK!";

class Callback: public DimInfo
{
public:
	bool first;
	string callbackName;
	
	Callback(string name): DimInfo(const_cast<char*>((name).c_str()), nofred)
	{
		callbackName = name;
		first = true;
		strcpy(nofred, "FRED CRASHED!");
	}
	
	void infoHandler()
	{
		if (first)
		{
			first = false;
			return;
		}
		else
		{
			error = false;
			string response = getString();
			if (response == "FRED CRASHED!")
			{
				PrintError("FRED CRASHED!");
				exit(-1);
			}
			else if (response == "NO DIM LINK!")
			{
				PrintError("NO DIM LINK!");
				exit(-1);
			}
			else if (response == "")
			{
				PrintError(callbackName + ": EMPTY RESPONSE RECEIVED!");
				exit(-1);
			}
			else
			{
				PrintInfo(callbackName + " received message:\n" + response);
			}
			waiting = false;
		}
	}
};

class ERRCallback: public DimInfo
{
public:
	bool first;
	string callbackName;
	
	ERRCallback(string name): DimInfo(const_cast<char*>((name + "_ERR").c_str()), nofred)
	{
		callbackName = name;
		first = true;
		strcpy(nofred, "FRED CRASHED!");
	}
	
	void infoHandler()
	{
		if (first)
		{
			first = false;
			return;
		}
		else
		{
			error = true;
			string response = getString();
			if (response == "FRED CRASHED!")
			{
				PrintError("FRED CRASHED!");
				exit(-1);
			}
			else if (response == "NO DIM LINK!")
			{
				PrintError("NO DIM LINK!");
				exit(-1);
			}
			else if (response == "")
			{
				PrintError(callbackName + ": EMPTY RESPONSE RECEIVED!");
				exit(-1);
			}
			else
			{
				PrintError(callbackName + " received error message:\n" + response);
			}
			waiting = false;
		}
	}
};

#endif // CALLBACKS_H
